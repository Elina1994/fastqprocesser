///* TESTCLASS
// * Copyright (c) 2016 Elina Wever [e.e.wever@st.hanze.nl].
// * All rights reserved.
// */
//package nl.bioinf.nl.eewever.fastq_processer;
//
//import java.io.BufferedReader;
//import java.io.IOException;
//import java.nio.charset.Charset;
//import java.nio.file.Files;
//import java.nio.file.Path;
//import java.nio.file.Paths;
//import java.util.HashMap;
//import java.util.Map;
//
///**
// * This class opens a fastq file and processes all reads.
// *
// * @author Elina Wever [e.e.wever@st.hanze.nl]
// * @version 0.0.1
// */
//public class ProcessReads {
//
//    /**
//     * Public constructor.
//     */
//    public ProcessReads() {
//
//    }
//
//    /**
//     * Converted PHRED score, integer.
//     */
//    private int score;
//    /**
//     * Total number of reads.
//     */
//    private int totalNumberOfReads;
//    /**
//     * Phred score of all reads.
//     */
//    private String phredScore;
//
//    /**
//     * This method processes all reads of a fastq file. It calculates the average per position for all reads
//     * This can also be used as a test class to check wether the output of the function is what was expected
//     * @param args file input
//     * @throws java.io.IOException when no file is available
//     */
//    public final void processFastqFile(final String[] args) throws IOException {
//        Path pathToFile = Paths.get(args[0]);
//        BufferedReader fastqReader = Files.newBufferedReader(pathToFile,
//                Charset.defaultCharset());
//
//        String fileLines;
//        //counts total number of lines
//        Integer lineCount = 0;
//        //enumerates each read counting from 1-4
//        Integer counter = 1;
//        HashMap<Integer, Integer> hm = new HashMap<>();
//        HashMap<Integer, Integer> mergedhm = new HashMap<>();
//
//        while ((fileLines = fastqReader.readLine()) != null) {
//            counter++;
//            lineCount++;
//
//            if (fileLines.startsWith("+")) {
//                counter = 0;
//            }
//            if (counter == 1) {
//                this.phredScore = fileLines;
//
//                //calculate phred score
//                for (int i = 0; i < fileLines.length(); i++) {
//                    char character = phredScore.charAt(i);
//                    //convert character to ascii
//                    this.score = character;
//                    //assign values to position
//                    int position = i + 1;
//                    //put all values + keys in a hashmap
//                    hm.put(position, this.score);
//                }
//                //loop through all entries of the hashmap and determine
//                //old values and sum each value per position.
//
////                hm.entrySet().stream().forEach((entry) -> {
//                for (Map.Entry<Integer,Integer> entry : hm.entrySet()) {
//                    int extractValues = entry.getValue();
//                    //extract the values of the keys of the old(existing) hashmap
//                    Integer mergedValues = mergedhm.get(entry.getKey());
//                    //if the element not already exists sum up each value
//                    //also avoids a nullpointer exception when the value does not
//                    //exist yet
//                    if (mergedValues != null) {
//                        extractValues = extractValues + mergedValues;
//                    }
//                    //put the sum per position in a new hashmap
//                    mergedhm.put(entry.getKey(), extractValues);
//                };
//
//            }
//
//        }
//
//        //count total number of reads
//        this.totalNumberOfReads = lineCount / 4;
////        mergedhm.entrySet().stream().forEach((items) -> {
//        for (Map.Entry<Integer,Integer> items: mergedhm.entrySet()) {
//            int averageScore = items.getValue() / totalNumberOfReads;
//            mergedhm.put(items.getKey(), averageScore);
//        };
//        for (Map.Entry<Integer,Integer> test: mergedhm.entrySet()) {
//            System.out.println(test.getKey() + "\t" + test.getValue());
//        };
////        System.out.println(mergedhm);
//    }
//
//}