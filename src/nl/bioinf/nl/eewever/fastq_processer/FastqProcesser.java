/*
 * Copyright (c) 2015 Elina Wever [e.e.wever@st.hanze.nl].
 * All rights reserved.
 */
package nl.bioinf.nl.eewever.fastq_processer;

import java.io.IOException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

/**
 * This class processes the Mapper plus Reducer.
 * The result is a file with position and average PHRED score.
 *
 * @author Elina Wever [e.e.wever@st.hanze.nl]
 * @version 0.0.1
 */
public final class FastqProcesser {

    /**
     * Private contstructor for FastqProcesser.
     */
    private FastqProcesser() {

    }

     /**
     * This is the main of the program.
     * It calls all classes : Mapper , Reducer and main method also the input and
     * output files are specified here
     *
     * @param args the command line arguments
     * @throws java.io.IOException if file not found.
     * @throws java.lang.InterruptedException if mapreduce job was interrupted
     * @throws java.lang.ClassNotFoundException if the mapreduce job fails
     */
    public static void main(final String[] args) throws IOException, InterruptedException, ClassNotFoundException {
        Configuration conf = new Configuration();
        Job job = Job.getInstance(conf, "process fastqfile");

        job.setJarByClass(FastqProcesser.class);
        job.setMapperClass(ReadMapper.class);
        job.setCombinerClass(ReadReducer.class);
        job.setReducerClass(ReadReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);
        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));

        System.exit(job.waitForCompletion(true) ? 0 : 1);

    }

}
