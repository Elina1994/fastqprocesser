/*
 * Copyright (c) 2015 Elina Wever [e.e.wever@st.hanze.nl].
 * All rights reserved.
 */
package nl.bioinf.nl.eewever.fastq_processer;

import java.io.IOException;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

/**
 * This class combines output from different mappers.
 *
 * @author Elina Wever [e.e.wever@st.hanze.nl]
 * @version 0.0.1
 */
public class ReadReducer extends Reducer<Text, IntWritable, Text, IntWritable> {

    /**
     * Shows combined results.
     * This class combines the results from the created mappers
     * the end results should be an n number of positions with their average PHRED score
     */
    private final IntWritable results = new IntWritable();

    @Override
    public void reduce(final Text key, final Iterable<IntWritable> values, final Context context)
            throws IOException, InterruptedException {

        int sum = 0;
        //iterate over created values
        for (IntWritable val : values) {
            //combine values
            sum += val.get();

        }
        this.results.set(sum);
        //write results to file
        context.write(key, this.results);

    }

}
