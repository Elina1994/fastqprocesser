/*
 * Copyright (c) 2015 Elina Wever [e.e.wever@st.hanze.nl].
 * All rights reserved.
 */
package nl.bioinf.nl.eewever.fastq_processer;

import java.io.IOException;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

/**
 * This class calculates the average PHRED score per position using a mapper function.
 *
 * @author eewever [e.e.wever@st.hanze.nl]
 * @version 0.0.1
 */
public class ReadMapper extends Mapper<Object, Text, Text, IntWritable> {

    /**
     * Converted PHRED score, integer.
     */
    private int score;
    /**
     * Total number of reads.
     */
    private int totalNumberOfReads;

    /**
     * Contains the position of the character.
     */
    private final Text position = new Text();

    /**
     * Keeps track of the number of lines.
     */
    private Integer lineCount;

    /**
     * Average PHRED score.
     */
    private IntWritable averageScore;

    @Override
    /**
     * This method maps the position plus the average PHRED score.
     *
     * @throws IOException when no input file was found.
     * @throws InterruptedException if the job was interrupted.
     */
    public void map(final Object key, final Text value, final Mapper.Context context)
            throws IOException, InterruptedException {
        //Get file lines and convert them to a String
        String fileLines = value.toString();

        StringBuilder convertPos = new StringBuilder();

        //(54146688)count total number of lines
        this.lineCount++;


        for (int i = 0; i < fileLines.length(); i++) {

            //convert character to ascii
            char character = fileLines.charAt(i);
            //calculate phred score
            this.score = character;
            convertPos.append(i + 1);
            //assign values to position
            this.position.set(convertPos.toString());
            //calculate total number of reads (sum of all the lines divided by 4)
            //because each read has 4 lines
            //54146688 total number of lines in big file
            this.totalNumberOfReads = this.lineCount / 4;
            //caluclate average PHRED score
            int average = this.score / this.totalNumberOfReads;
            //convert integer to IntWritable
            this.averageScore = new IntWritable(average);

        }
        //Write output to file
        context.write(this.position, this.averageScore);
    }
}
