# README #

### What is this repository for? ###

This repository contains a fastq file processer.
It calculates the average PHRED score per position.

This program is implemented in map and reduce style.
It contains a Mapper and a Reduce class.

### How do I get set up? ###

*Install kerberos
* then use kinit username

submit your jobs by using the following command
yarn jar "path to .jar file" "path to input file" "path to output file"


### Who do I talk to? ###

Elina Wever [e.e.wever@st.hanze.nl]